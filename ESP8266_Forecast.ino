/*The MIT License (MIT)

  Copyright (c) 2018 by ThingPulse Ltd., https://thingpulse.com

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <Arduino.h>
#include <SPI.h>               // include Arduino SPI library
#include <Adafruit_GFX.h>      // include adafruit graphics library
#include <Adafruit_PCD8544.h>  // include adafruit PCD8544 (Nokia 5110) library
#include <ESP8266WiFi.h>
#include <JsonListener.h>
#include <time.h>
#include "OpenWeatherMapForecast.h"
#define MAISON
#include "config.h"

// Nokia 5110 LCD module connections (CLK, DIN, D/C, CS, RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(D4, D3, D2, D1, D0);



String OPEN_WEATHER_MAP_APP_ID = String(myAPIKEY);
String OPEN_WEATHER_MAP_LOCATION_ID = "2998324";
/*
  Arabic - ar, Bulgarian - bg, Catalan - ca, Czech - cz, German - de, Greek - el,
  English - en, Persian (Farsi) - fa, Finnish - fi, French - fr, Galician - gl,
  Croatian - hr, Hungarian - hu, Italian - it, Japanese - ja, Korean - kr,
  Latvian - la, Lithuanian - lt, Macedonian - mk, Dutch - nl, Polish - pl,
  Portuguese - pt, Romanian - ro, Russian - ru, Swedish - se, Slovak - sk,
  Slovenian - sl, Spanish - es, Turkish - tr, Ukrainian - ua, Vietnamese - vi,
  Chinese Simplified - zh_cn, Chinese Traditional - zh_tw.
*/
String OPEN_WEATHER_MAP_LANGUAGE = "fr";
boolean IS_METRIC = true;
uint8_t MAX_FORECASTS = 8;
const int UPDATE_INTERVAL_SECS = 20 * 60; // Update every 20 minutes

OpenWeatherMapForecastData data[8];
// initiate the client
OpenWeatherMapForecast forecastClient;
uint8_t foundForecasts;
bool readyForWeatherUpdate = false;
String lastUpdate = "--";
long timeSinceLastWUpdate = 0;

/**
   WiFi Settings
   */
   
const char* ESP_HOST_NAME = "esp-" + ESP.getFlashChipId();

// initiate the WifiClient
WiFiClient wifiClient;


void updateData() {

  display.clearDisplay();   // clear the screen and buffer
  display.setCursor(0, 20);
  display.print("  ... ... ...  " );
  display.display();
  foundForecasts = forecastClient.updateForecastsById(data, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION_ID, MAX_FORECASTS);

  readyForWeatherUpdate = false;
  delay(1000);
}
/**
   SETUP
*/
void setup() {
  Serial.begin(115200);

  display.begin();
  display.setContrast(50);

  connectWifi();
  delay(3000);

  display.clearDisplay();   // clear the screen and buffer
  display.setCursor(0, 20);
  display.print("  ... ... ...  " );
  display.display();

  forecastClient.setMetric(IS_METRIC);
  forecastClient.setLanguage(OPEN_WEATHER_MAP_LANGUAGE);
  // Keep in mind : every 3 hours only (0,3,6,9,12 ...)
  uint8_t allowedHours[] = {9, 15};
  forecastClient.setAllowedHours(allowedHours, 2);
  foundForecasts = forecastClient.updateForecastsById(data, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION_ID, MAX_FORECASTS);

}


/**
   LOOP
*/
void loop() {

  if (millis() - timeSinceLastWUpdate > (1000L * UPDATE_INTERVAL_SECS)) {
    setReadyForWeatherUpdate();
    timeSinceLastWUpdate = millis();
  }

  if (readyForWeatherUpdate ) {
    updateData();
  }

  for (uint8_t i = 0; i < foundForecasts; i++) {
    Serial.printf("---\nForecast number: %d\n", i);
    // {"dt":1527066000, uint32_t observationTime;
    // "main":{
    time_t time;

    time = data[i].observationTime;
    Serial.printf("observationTime: %d, full date: %s", data[i].observationTime, ctime(&time));

    //   "temp":17.35, float temp;
    Serial.printf("temp: %f\n", data[i].temp);
    //   "temp_min":16.89, float tempMin;
    Serial.printf("tempMin: %f\n", data[i].tempMin);
    //   "temp_max":17.35, float tempMax;
    Serial.printf("tempMax: %f\n", data[i].tempMax);
    Serial.printf("pressure: %f\n", data[i].pressure);
    Serial.printf("humidity: %d\n", data[i].humidity);
    //   "id":802, uint16_t weatherId;
    Serial.printf("weatherId: %d\n", data[i].weatherId);
    //   "main":"Clouds", String main;
    Serial.printf("main: %s\n", data[i].main.c_str());

    //   "description":"scattered clouds", String description;
    Serial.printf("description: %s\n", data[i].description.c_str());
    //   "icon":"03d" String icon; String iconMeteoCon;
    Serial.printf("icon: %s\n", data[i].icon.c_str());
    Serial.printf("iconMeteoCon: %s\n", data[i].iconMeteoCon.c_str());
    // }],"clouds":{"all":44}, uint8_t clouds;
    Serial.printf("clouds: %d\n", data[i].clouds);
    // "wind":{
    //   "speed":1.77, float windSpeed;
    Serial.printf("windSpeed: %f\n", data[i].windSpeed);
    //   "deg":207.501 float windDeg;
    Serial.printf("windDeg: %f\n", data[i].windDeg);
    // rain: {3h: 0.055}, float rain;
    Serial.printf("rain: %f\n", data[i].rain);
    // },"sys":{"pod":"d"}
    // dt_txt: "2018-05-23 09:00:00"   String observationTimeText;
    Serial.printf("observationTimeText: %s\n", data[i].observationTimeText.c_str());
    String _fTime = data[i].observationTimeText.substring(8, 10) + "/" + data[i].observationTimeText.substring(5, 7) + "  "+ data[i].observationTimeText.substring(11,14)+"h";
    display.clearDisplay();   // clear the screen and buffer
    display.setCursor(12 , 0);
    display.print( _fTime);

    display.drawFastHLine(0, 8, display.width(), BLACK);
    display.setCursor(0, 10);

    display.print( data[i].description);
    display.setCursor(0, 20);
    display.print( String(int(data[i].tempMin)));
    display.drawRect(15, 20, 3, 3, BLACK);

    display.setCursor(0, 28);
    display.print( String(int(data[i].tempMax)));
    display.drawRect(15, 28, 3, 3, BLACK);

    display.setCursor(35, 20);
    display.print( String(data[i].rain) + "mm");



    display.display();
    delay(4000);

  }

}



void connectWifi() {
  // initialize the display
  Serial.print("Connecting to");
  Serial.println(mySSID);
  display.clearDisplay();   // clear the screen and buffer

  display.drawFastHLine(0, 23, display.width(), BLACK);

  display.print("Connecting to :");
  display.setCursor(6, 8);
  display.print(mySSID);
  display.display();

  delay(500);
  WiFi.begin(mySSID, myPASSWORD);


  display.setTextSize(1);
  display.setTextColor(BLACK, WHITE);
  display.setCursor(6, 0);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  display.clearDisplay();   // clear the screen and buffer

  display.setCursor(6, 0);
  display.print("Connected!" );

  Serial.println("");
  Serial.println("Connected!");
  display.setCursor(6, 16);
  Serial.println(WiFi.localIP());
  display.print(WiFi.localIP());
  Serial.println();
  display.display();
  delay(3000);
}


void setReadyForWeatherUpdate() {
  Serial.println("Setting readyForUpdate to true");
  readyForWeatherUpdate = true;
}
